#!/bin/bash

#
# clean up
#
if [[ ! -z $1 ]]; then

    for f in `ls -1 * | grep -v sh | grep -v bib`
    do
        rm -f $f
    done

exit 0

fi




bibtex2x   -i test.bib -o test_j.md -t journal.markdown

bibtex2x   -i test.bib -o test_c.md -t conf.markdown

bibtex2x   -i test.bib -o test.tex  -t latex.tex              \
                    --variable section="Chunhua Shen"                                \
                    --variable footerlink="http://cs.adelaide.edu.au/users/chhshen/" \
                    --variable titlecolor=blue \
                    --variable selected_conferences="NIPS, ICML, ICCV, CVPR, ECCV"

bibtex2x   -i test.bib -o test2.tex  -t latex.tex              \
                    --variable section="Chunhua Shen"                                \
                    --variable footerlink="http://cs.adelaide.edu.au/users/chhshen/" \
                    --variable titlecolor=red

bibtex2x   -i test.bib -o test3.tex                \
                    --variable section="Chunhua Shen"                                \
                    --variable footerlink="http://cs.adelaide.edu.au/users/chhshen/" \
                    --variable titlecolor=blue


if [[ `type lualatex` ]]
then
    for f in `ls -1 *tex`
    do
        echo "lualatex-ing: "$f
        lualatex  $f    > /dev/null
    done
fi



