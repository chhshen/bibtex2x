
from setuptools import find_packages, setup
from src import __version__

"""
BibTeX2x is a simple tool to convert BibTeX into HTML, Markdown, plain text, PDF,
and many other types of documents based on Jinjia2 templates.
"""

setup(
    name='bibtex2x',
    version= str(__version__),
    packages = ['bibtex2x'],
    package_dir = {'bibtex2x': 'src'},

    scripts =['script/aux2bibtex', 'script/tidybib', 'script/aux2bbl'],

    author = 'Chunhua Shen',
    author_email = 'chhshen@gmail.com',
    url = 'https://bitbucket.org/chhshen/bibtex2x',
    long_description = 'Converting BibTeX files into other documents using templates',
    license = 'Mozilla',

    include_package_data = True,

    entry_points = {
        'console_scripts': 'bibtex2x = bibtex2x.bibtex2x:main'
    },
    install_requires = [
        'Jinja2 >= 2.7',
        'pybtex >= 0.16',
    ],
    platforms = 'any',
    zip_safe = False,

    classifiers = [
        'Development Status :: 1.0',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: Mozilla License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Topic :: LaTeX',
        'Topic :: Text Processing',
        'Topic :: Utilities',
    ]
)

