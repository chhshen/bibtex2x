from os import path as op
import types
import re

def _cleanpath(*args):
    parts = [args[0].strip()]

    for arg in args[1:]:
        parts.append((arg.replace(op.sep, '', 1) if arg.startswith(op.sep) else arg).strip())

    return parts


def abspath(*args):
    return op.realpath(
        op.expanduser(
            op.join(
                *_cleanpath(*args)
            )
        )
    )


def normpath(*args):
    return op.normpath(
        op.join(
            *_cleanpath(*args)
        )
    )


def str2list(strin):
#   input a string like "iccv; eccv; accv; cvpr"
#   return an iterable list
    if strin is None or isinstance(strin, types.NoneType):
        return strin

    _z = strin.split(',')
    _t = []
    for _i in _z:
        _t.append(_i.strip())

    return _t


def remove_curly_bracket(x):
    z = x.replace('{', '')
    z2 = z.replace('}', '')
    return z2


def initialize_name(x):
# x is in the format of "Chunhua Shen, X XX ..."
# return "C. Shen, X. XX"
    change = 1
    s = ''
    for i in range(len(x)):

        if change == -1 and i < len(x) and x[i] == " ":
            change = 0

        if change == 0:
            s = s + x[i]

        if change == 1 and x[i] != " ":
            s = s + x[i]
            s += "."
            change = -1 # ignore the rest of the given name

        if x[i] == "," and x[i + 1] == " ":
            change = 1
            s = s + " "
    return s


def clear_comments(data):
    """Return the bibtex content without comments"""
    res = re.sub(r"(%.*\n)", '', data)
    res = re.sub(r"(comment [^\n]*\n)", '', res)
    return res


def capitalize_string(x):
    y = ''
    transform = 1
    for i in range(len(x)):
        #
        # This code will remove curly brackets in the title
        #
        # if x[i] == "{":
        #    transform = 0
        #    continue
        #
        # if x[i] == "}":
        #    transform = 1
        #    continue
        #

        if x[i] == "{":
            transform = 0

        if x[i] == "}":
            transform = 1

        if transform:
            y = y + x[i].lower()
        else:
            y = y + x[i]

        z = ''
        z = z + y[0].upper()
        z = z + y[1:len(y)]
    return z

def filetype(filename):
    extension_name = op.splitext(filename)[1][1:].strip().lower()

    if extension_name == 'md' or extension_name == 'markdown':
        return 'markdown'

    if extension_name == 'html' or extension_name == 'htm':
        return 'html'

    if extension_name in [ 'tex', 'latex', 'ltx' ]:
        return 'latex'

    if extension_name == 'pdf':
        return 'pdf'

    if extension_name == 'txt':
        return 'txt'

    if extension_name == 'doc':
        return 'doc'

    if extension_name == 'docx':
        return 'docx'

    if extension_name == 'bib':
        return 'bibtex'

    if extension_name == 'bbl':
        return 'bbl'

    if extension_name == 'xml':
        return 'xml'

    return 'unknown'

