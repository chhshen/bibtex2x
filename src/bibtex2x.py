#
# You need to install pybtex first, just run `pip install pybtex'
# Chunhua Shen,  26 August, 2013
#


import types

from    pybtex.database.input import bibtex

from    renderers import Renderer
from    fs import Directory, File
from    utils import *


#
# Global variables
#
template_vars = {}
sorting_kind = 'year'


#
# sorting by year
#
def sort_list_cmp(y, x):
    yearcmp = int(x[1].fields['year']) - int(y[1].fields['year'])
    if yearcmp != 0 and not isinstance( yearcmp, types.NoneType):
        return yearcmp

    if 'journal' in x[1].fields and 'journal' not in y[1].fields:
        return 1
    if 'journal' not in x[1].fields and 'journal' in y[1].fields:
        return -1

    if 'booktitle' in x[1].fields and 'booktitle' not in y[1].fields:
        return 1
    if 'booktitle' not in x[1].fields and 'booktitle' in y[1].fields:
        return -1

    if 'venue' in x[1].fields and 'venue' in y[1].fields:
        if x[1].fields['venue'] > y[1].fields['venue']:
            return -1

    if 'venue' in x[1].fields and 'venue' in y[1].fields:
        if x[1].fields['venue'] < y[1].fields['venue']:
            return 1

    if 'author' in x[1].fields and 'author' in y[1].fields:
        if x[1].fields['author'] > y[1].fields['author']:
            return 1
        else:
            return -1

    # always return something
    return 1

#
# sorting by type
#
def sort_list_cmp2(y, x):
    if 'journal' in x[1].fields and 'journal' not in y[1].fields:
        return 1
    if 'journal' not in x[1].fields and 'journal' in y[1].fields:
        return -1

    if 'booktitle' in x[1].fields and 'booktitle' not in y[1].fields:
        return 1
    if 'booktitle' not in x[1].fields and 'booktitle' in y[1].fields:
        return -1

    if 'year' in x[1].fields and 'year' in y[1].fields:
        yearcmp = int(x[1].fields['year']) - int(y[1].fields['year'])
    else:
        yearcmp = 1

    if yearcmp != 0 and not isinstance( yearcmp, types.NoneType):
        return yearcmp

    if 'venue' in x[1].fields and 'venue' in y[1].fields:
        if x[1].fields['venue'] > y[1].fields['venue']:
            return -1
        if x[1].fields['venue'] < y[1].fields['venue']:
            return 1

    if 'author' in x[1].fields and 'author' in y[1].fields:
        if x[1].fields['author'] > y[1].fields['author']:
            return 1
        else:
            return -1

    # always return something
    return 1


def load_citekey(bib_data, citekey):
# Get the info corresponding to specific citekey
    entry = bib_data.entries[citekey]
    return entry


def gen_citefields(bib_data, citekey):
# Rearrange the bib entries into something more useful.
    entry = load_citekey(bib_data, citekey)
    D = entry.fields

    authors = [" ".join(p.first() + p.prelast() + p.last()) for p in entry.persons['author']]
    D['authors'] = ", ".join(authors)
    return D


# def instance_field(x):
#    if x in value.fields and len(value.fields[x]) > 0:
#        return " " + x + " " + value.fields[x] + ", "
#    else:
#        return ""


def instance_fields(x, fields):
    if x in fields and len(fields[x]) > 0:
        return fields[x]
    else:
        return ""


def render(variables, template_file):
    src = Directory('.')
    _rd = Renderer(src.path)

    # if the template_file is LaTeX/bibtex, we use different Jinja delimiters
    if  filetype(template_file) in ['latex', 'bbl']:
        _rd.latex_env(src.path)

    _rd.environment.filters['str2list'] = str2list
    _rd.environment.filters['remove_curly_bracket'] = _rd.environment.filters

    return _rd.render(template_file, variables)


#
# core function to generate the output
#
def generate(bib_filename, output_filename, template_file):
    """

    :param bib_filename:
    :param output_filename:
    :param template_file:
    """
    print("input bibtex: " + bib_filename)
    print("jinjia2 template file: " + template_file)

    # src = Directory('.')

    # f = File(normpath(src.path, yaml_config_file ))
    # if f.exists:
    #    config_data = yaml.load(f.content)
    # else:
    #    print("Yaml config file not found. Abort.")
    #    sys.exit(-1)

    parser = bibtex.Parser()
    bib_data = parser.parse_file(bib_filename)

    # num_papers = len(bib_data.entries)

    if sorting_kind == 'type':
        bib_sorted = sorted(bib_data.entries.items(), cmp=sort_list_cmp2)
    else:
        bib_sorted = sorted(bib_data.entries.items(), cmp=sort_list_cmp)

    _paper_title = []
    _authors = []
    _year = []
    _venue = []
    _journal = []
    _booktitle = []
    _publisher = []
    _eprint = []
    _pdf = []
    _abstract = []
    _url = []
    _vol = []
    _number = []
    _pages = []
    _note = []
    _key = []
    _project = []
    _type = []
    _editor = []
    _address = []


    #idx=-1;
    for key, value in bib_sorted:
    #    idx = idx + 1

        _key.append(key)

        _type.append( str( value.type ) )

        _t1 = value.fields['title']
        _t1 = capitalize_string(_t1)
        # print ". *" + t1 + "*.  "
        _paper_title.append(_t1)

        citef = gen_citefields(bib_data, key)
        # this can be done in the jinja templates
        # _t2 = remove_curly_bracket(instance_fields('authors', citef))

        _t2 = initialize_name( instance_fields('authors', citef) )
        _authors.append(_t2)

        if 'year' in value.fields:
            _year.append(value.fields['year'])
        else:
            _year.append("")

        if 'venue' in value.fields:
            _venue.append(value.fields['venue'])
        else:
            _venue.append("")

        if 'journal' in value.fields:
            _journal.append(value.fields['journal'])
        else:
            _journal.append("")

        if 'booktitle' in value.fields:
            _booktitle.append(value.fields['booktitle'])
        else:
            _booktitle.append("")

        if 'publisher' in value.fields:
            _publisher.append(value.fields['publisher'])
        else:
            _publisher.append("")

        if 'eprint' in value.fields and len(value.fields['eprint']) > 0:
            _eprint.append(value.fields['eprint'])
        else:
            _eprint.append("")

        if 'url' in value.fields and len(value.fields['url']) > 0:
            _url.append(value.fields['url'])
        else:
            _url.append("")

        if 'pdf' in value.fields and len(value.fields['pdf']) > 0:
            _pdf.append(value.fields['pdf'])
        else:
            _pdf.append("")

        if 'volume' in value.fields:
            _vol.append(value.fields['volume'])
        else:
            _vol.append("")

        if 'number' in value.fields:
            _number.append(value.fields['number'])
        else:
            _number.append("")

        if 'pages' in value.fields:
            _pages.append(value.fields['pages'])
        else:
            _pages.append("")

        if 'note' in value.fields:
            _note.append(value.fields['note'])
        else:
            _note.append("")

        if 'project' in value.fields:
            _project.append(value.fields['project'])
        else:
            _project.append("")

        if 'editor' in value.fields:
            _editor.append(value.fields['editor'])
        else:
            _editor.append("")

        if 'address' in value.fields:
            _address.append(value.fields['address'])
        else:
            _address.append("")

        if 'abstract' in value.fields:
            _abstract.append(value.fields['abstract'])
        else:
            _abstract.append("")



    global template_vars

    template_vars['title'] = _paper_title
    template_vars['authors'] = _authors
    template_vars['year'] = _year
    template_vars['venue'] = _venue
    template_vars['journal'] = _journal
    template_vars['booktitle'] = _booktitle
    template_vars['publisher'] = _publisher
    template_vars['eprint'] = _eprint
    template_vars['pdf'] = _pdf
    template_vars['abstract'] = _abstract
    template_vars['url'] = _url
    template_vars['volume'] = _vol
    template_vars['number'] = _number
    template_vars['pages'] = _pages
    template_vars['note'] = _note
    template_vars['key'] = _key
    template_vars['project'] = _project
    template_vars['type'] = _type
    template_vars['editor'] = _editor
    template_vars['address'] = _address


    rendered = render(template_vars, template_file)

    # create file using rendered content
    out = File(output_filename)
    if out.exists:
        out.rm()
    File(output_filename, rendered).mk()

    print("output: " + output_filename)


def main():
    import argparse

    global sorting_kind
    global template_vars

    ap = argparse.ArgumentParser(description='bib2x: Converting bibtex to other formats.')
    ap.add_argument('-i', '--input', help='input bibtex for processing (required)', required=True)
    ap.add_argument('-o', '--output', help='output file (required)', required=True)

    ap.add_argument('-t', '--template', help='user-provided template file. if not specified,\
            the default one will be used.',
                    required=False)

    ap.add_argument('-s', '--sort', help='sort the bibtex entries according to year or type. \
                    if not specified, the default is sorting according to the published year.  \
                    \'-s type/year\' explicitly specifies sorting according to type or year.',
                    required=False)

    ap.add_argument('-v', '--variable', help='variables passed to the template file.', required=False, nargs='*',
                    action='append')

    args = vars(ap.parse_args())

    _tvar = args['variable']
    if not isinstance(_tvar, types.NoneType):
        for i in _tvar:
            _str = i[0]
            [_str1, _str2] = _str.split('=', 1)
            _str1 = _str1.strip(' \t\n\r')
            _str2 = _str2.strip(' \t\n\r')
            # pass template variables
            template_vars[_str1] = _str2

    input_bibfile = args['input']

    output_filename = args['output']

    _sorting_kind = args['sort']
    if not isinstance(_sorting_kind, types.NoneType):
        sorting_kind = _sorting_kind

    # print template_vars
    # template file
    _ft = filetype(output_filename)

    template_file = 'html.markdown'
    if _ft == 'markdown':
        template_file = 'journal.markdown'
    if _ft == 'latex':
        template_file = 'latex.tex'
    if _ft == 'bibtex':
        template_file = 'bibtex.bib'

    _tf = args['template']
    if not isinstance(_tf, types.NoneType):
        template_file = _tf

    generate(input_bibfile, output_filename, template_file)


if __name__ == "__main__":
    main()

