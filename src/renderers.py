from jinja2 import Environment, FileSystemLoader, PrefixLoader
from jinja2.exceptions import TemplateNotFound

import os


class Renderer(object):
    def __init__(self, path):


        # system install dir.
        _installation_dir = os.path.realpath(__file__)
        _real_ins_dir = os.path.dirname( _installation_dir )

        self.environment = Environment(
            loader=FileSystemLoader(
                [path, path + os.sep + "_templates", _real_ins_dir + os.sep + "_templates"] )
        )

    def render(self, template, vars_={}):
        try:
            template = self.environment.get_template(template)

        except TemplateNotFound:
            print( 'Template not found.' )
            raise

        return template.render(**vars_)


    def latex_env(self, path):
        #
        # LaTeX template
        #
        # system install dir.
        _installation_dir = os.path.realpath(__file__)
        _real_ins_dir = os.path.dirname( _installation_dir )


        self.environment = Environment(
            block_start_string='((#',
            block_end_string='#))',
            variable_start_string='((',
            variable_end_string='))',
            loader=FileSystemLoader(
                [path, path + os.sep + "_templates", _real_ins_dir + os.sep + "_templates"] )
        )


