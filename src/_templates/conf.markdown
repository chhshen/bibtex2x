{% for t in title -%}
{% if booktitle[loop.index-1] != "" -%}
* {{ t }}
  * {{ authors[loop.index-1] }}
  * In: {{ booktitle[loop.index-1] }} ({{ year[loop.index-1] }})
{% endif  -%}
{% endfor -%}
