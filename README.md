BibTeX2x
========
Converting a bibtex file into HTML, markdown, LaTeX, PDF etc.

This project uses Mozilla Public License (http://www.mozilla.org/MPL/)


Install
--------
You can install this package using `pip': 
(git and pip need to be installed before run this command):

~~~
pip install git+https://bitbucket.org/chhshen/bibtex2x/
~~~

